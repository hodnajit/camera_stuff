import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.actions import Node


def generate_launch_description():
    tag_pose_publish_dir = get_package_share_directory('tag_pose_publish')
    tag_pose_publish_launch_dir = os.path.join(tag_pose_publish_dir, 'launch')

    return LaunchDescription([
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                os.path.join(tag_pose_publish_launch_dir, 'april_tag_detection_launch.py'))),
        Node(
            package='tag_pose_publish',
            executable='tag_pose_publish',
        ),
    ])
