#include <chrono>
#include <functional>
#include <memory>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "tf2_msgs/msg/tf_message.hpp"

class TagPosePublisher : public rclcpp::Node
{
  public:
    TagPosePublisher()
    : Node("tag_pose_publisher")
    {
      publisher_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("tag/pose", 10);
      tf_subscription_ = this->create_subscription<tf2_msgs::msg::TFMessage>(
        "tf", 10, std::bind(&TagPosePublisher::tf_callback_, this, std::placeholders::_1));
      geometry_msgs::msg::Transform transform;
      transform.translation.x = 1.318;
      transform.translation.y = 2.399;
      transform.translation.z = 2.625;
      transform.rotation.x = 0.707107;
      transform.rotation.y = -0.707107;
      transform.rotation.z = 1.26918e-09;
      transform.rotation.w = -1.26918e-09;
      tf2::fromMsg(transform, world_to_camera);
      is_world_to_camera_init = true;
    }

  private:
    rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr publisher_;
    rclcpp::Subscription<tf2_msgs::msg::TFMessage>::SharedPtr tf_subscription_;
    tf2::Transform world_to_camera;
    bool is_world_to_camera_init{false};

    void publish_tag_position_(const geometry_msgs::msg::Transform world_to_tag) const {
      geometry_msgs::msg::PoseStamped tag_pose_msg;
      tag_pose_msg.pose.position.x = world_to_tag.translation.x;
      tag_pose_msg.pose.position.y = world_to_tag.translation.y;
      tag_pose_msg.pose.position.z = world_to_tag.translation.z;
      tag_pose_msg.pose.orientation.x = world_to_tag.rotation.x;
      tag_pose_msg.pose.orientation.y = world_to_tag.rotation.y;
      tag_pose_msg.pose.orientation.z = world_to_tag.rotation.z;
      tag_pose_msg.pose.orientation.w = world_to_tag.rotation.w;
      tag_pose_msg.header.frame_id = "world";
      tag_pose_msg.header.stamp = this->now();
      publisher_->publish(tag_pose_msg);
    }

    void tf_callback_(const tf2_msgs::msg::TFMessage::SharedPtr tf_msg)
    {
      auto transforms = tf_msg->transforms;
      for (geometry_msgs::msg::TransformStamped transform : transforms) {
       if (transform.header.frame_id == "camera_frame" && transform.child_frame_id == "tag36h11:0") {
         if (is_world_to_camera_init) {
           tf2::Transform tmp;
           geometry_msgs::msg::Transform world_to_tag;
           tf2::Transform camera_to_tag;
           tf2::fromMsg(transform.transform, camera_to_tag);
           tmp = world_to_camera * camera_to_tag;
           world_to_tag = tf2::toMsg(tmp);
           publish_tag_position_(world_to_tag);
         }
         else {
           RCLCPP_INFO(this->get_logger(), "Static transform between world and camera was not received.");
         }
       }
      }
    }

    void tf_static_callback_(const tf2_msgs::msg::TFMessage::SharedPtr tf_msg)
    {
      if (!is_world_to_camera_init) {
        auto transforms = tf_msg->transforms;
        for (auto transform : transforms) {
          if (transform.header.frame_id == "world" && transform.child_frame_id == "camera_frame") {
            tf2::fromMsg(transform.transform, world_to_camera);
            is_world_to_camera_init = true;
          }
        }
      }
    }
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<TagPosePublisher>());
  rclcpp::shutdown();
  return 0;
  return 0;
}
